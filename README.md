# notas_circuitos_digitais

### Aula 05 - Códigos Binários
 - Código BCD - Binary-coded decimal;
 - Código Gray 
 -- RBC - reflected binary code
 -- RB - reglected binary
 - Código Johnson
 - Código Excesso de 3
 - Código ASCII
 
### Aula 06 - Aritmética Binária
 - Adição de Números em Binário
 - Subtração de Números em Binário
 - Multiplicação de Números em Binário
 - Divisão de Números em Binário
 
### Aula 07 - Representação de Números Binários Inteiros com Sinal
 - Representação Sinal-Magnitude
 - Representação Complemento de 2
 - Aritmética na Representação Complemento de 2
 - Overflow Aritmético
 
### Aula 08 - Aritmética no sistema BCD
 - Adição de Números em BCD
 - Subtração de Números em BCD
 
### Aula 09 - Ferramentas de Descrição de Circuitos Digitais
 - Relações Algébricas de Circuitos Digitais
 - Classes de Circuitos Digitais
 - Tabela da Verdade
 - Diagrama de Temporização
 - Expressão Lógica
 - Diagrama de Circuito lógico

### Aula 10 - Funções e Portas Lógicas
 - Função e Porta OR
 - Função e Porta AND
 - Função e Porta NOT
 - Função e Porta NOR
 - Função e Porta NAND
 - Função e Porta XOR
 - Função e Porta XNOR
 
### Aula 11 - Propriedades da Álgebra Booleana
 - Identidades das Portas AND, OR, NOT e XOR
 - Propriedades Comutativa, Associativa e Distributiva
 - Teorema de De'Morgan
 
### Aula 12 - Análise de Expressões Lógicas
 - Cálculo de Saída de uma Expressão Lógica
 - Obtenção da Tabela da Verdade
 
### Aula 13 - Análise de Circuitos Combinacionais
 - Obtenção da Expressão Lógica e Tabela da Verdade
   a partir do Diagrama de Circuito
   
### Aula 14 - Elaboração de Diagrama de Circuito Lógico
   

## Referências
- Playlist:
https://youtube.com/playlist?list=PLXyWBo_coJnMYO9Na3t-oYsc2X4kPJBWf


